--first step--
CREATE DATABASE NewsDatabase

--second step--
USE NewsDatabase

CREATE TABLE News(
  id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  title NVARCHAR (300)		NOT NULL,
  news_text  NVARCHAR(MAX)  NOT NULL,
  news_date  DATETIME
);

CREATE TABLE Words(
  id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  word NVARCHAR (50)		NOT NULL,
  news_id INT FOREIGN KEY REFERENCES News(id)
);