﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using ApplicationCore.Entities.AppEntities;
using ApplicationCore.Entities.Interfaces;

namespace NewsApi.Controllers
{
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly INewsRepository _newsRepository;

        public NewsController(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        [HttpGet("api/posts")]
        public ActionResult<IEnumerable<News>> GetNewsWithDateFilter(DateTime dateTimeFrom, DateTime dateTimeTo)
        {
            try
            {
                return _newsRepository.GetDataWithDateFilter(dateTimeFrom, dateTimeTo);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return StatusCode(500);
            }
            
            
        }

        [HttpGet("api/search")]
        public ActionResult<IEnumerable<News>> GetNewsWithSearch(string text)
        {
            try
            {
                if (string.IsNullOrEmpty(text))
                    return BadRequest();
                return _newsRepository.GetDataWithSearch(text);
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
                return StatusCode(500);
            }
        }

        [HttpGet("api/topten")]
        public ActionResult<IEnumerable<Word>> GetTopTenWordsInNews()
        {
            try
            {
                return _newsRepository.GetTopTenWords();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return StatusCode(500);
            }
        }
    }

    
}
