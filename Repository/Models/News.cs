﻿using System;

namespace Repository.Models
{
    public class News
    {
        public News(string title, string text, DateTime date)
        {
            Title = title;
            Text = text;
            Date = date;
        }
        public string Title { get; private set; }
        public string Text { get; private set; }
        public DateTime Date { get; private set; }
    }
}
