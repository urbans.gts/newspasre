﻿using Microsoft.Data.SqlClient;
using Repository.Heplers;
using Repository.Models;
using System;
using System.Collections.Generic;

namespace Repository
{
    public class NewsParserRepository : BaseRepository
    {
        public void AddNewsToDatabase(List<News> news)
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    foreach (var item in news)
                    {
                        string query = @"INSERT INTO dbo.News(title, news_text, news_date)
                                VALUES(@title,@news_text,@news_date)";
                        using (var command = new SqlCommand(query, connection))
                        {
                            command.Parameters.Add(new SqlParameter("@title", $"{item.Title}"));
                            command.Parameters.Add(new SqlParameter("@news_text", $"{item.Text}"));
                            command.Parameters.Add(new SqlParameter("@news_date", $"{item.Date}"));
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }

        public void SendRequestForTextParsingIntoDataBase()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    var cmd = new SqlCommand(@"INSERT INTO​ [dbo].[Words]
                                        ​SELECT
                                        t.VALUE,
                                        n.id
                                        ​FROM [dbo].[News] AS n
                                        ​​CROSS APPLY​(SELECT * FROM STRING_SPLIT(n.news_text,' ')) AS t
                                        ​WHERE NOT EXISTS (SELECT * FROM [dbo].[Words] AS w
                                        ​ ​ ​ ​ ​ ​				WHERE w.news_id = n.id)", connection);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (SqlException e)
            {
                throw e;
            }
        }
        public DateTime GetLastDateTime()
        {
            try
            {
                DateTime date = default;
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    string query = "SELECT news_date FROM News ORDER BY news_date DESC";
                    using (var command = new SqlCommand(query, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                date = Helper.ConvertDateToString(reader["news_date"].ToString());
                                return date;
                            }
                        }
                    }
                }
                return date;
            }
            catch (SqlException e)
            {
                throw e;
            }

        }
    }
}

