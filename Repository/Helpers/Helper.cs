﻿using System;
using System.Net;
namespace Repository.Heplers
{
    public class Helper
    {
        public static DateTime ConvertDateToString(string date)
        {
            try
            {
                return DateTime.Parse(date);
            }
            catch (FormatException)
            {
                Console.WriteLine("Unable to parse the specified date!");
            }
            return default;
        }
        public static bool СheckWebsiteAvailability()
        {
            try
            {
                WebRequest request = WebRequest.Create($"https://tengrinews.kz/kazakhstan_news");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                return (response != null || response.StatusCode == HttpStatusCode.OK);
            }
            catch (Exception)
            {
                Console.WriteLine("No internet connection!");
                return false;
            }

        }
    }
}
