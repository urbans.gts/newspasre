## Что требуется сделать
1. Для запуска требуется MS SQL Server
2. К проету приложен sql файл CreateDatabaseAndTablessql, требуется открыть его и выполнить команды по созданию БД и таблиц
3. Далее требуется запустить консольное приложение NewsParser (данные будут обработаны и записаны в БД)
4. Далее требуется запустить Веб приложение NewApi (Если использовать Rider)
5. Если установлена Visual studio, то в свойствах проекта -> запускаемый проект ->NewsApi(запуск),NewsParser(Запуск)
