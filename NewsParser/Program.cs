﻿using Repository;
using Repository.Heplers;
using System;
using System.Threading.Tasks;

namespace NewsParser
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            while (true)
            {
                RunParser();
                await Task.Delay(10800000);//ожидание 3 часа
            }
            
        }

        static void RunParser()
        {            
            if (Helper.СheckWebsiteAvailability())
            {
                Console.WriteLine($"Start parsing {DateTime.Now}");
                var repo = new NewsParserRepository();
                Parser parser = new();
                parser.StartParse(repo.GetLastDateTime());
                repo.AddNewsToDatabase(parser.NewsCollection);
                repo.SendRequestForTextParsingIntoDataBase();
            }
            else
            {
                Console.WriteLine("Website unavailable!");
            }
        }
    }
}
