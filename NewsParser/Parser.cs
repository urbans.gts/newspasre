﻿using Repository;
using Repository.Heplers;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;

namespace NewsParser
{
    internal class Parser
    {
        private readonly string regexLink = "<a href=(.*?) class=\"tn-link\"";
        private readonly string regexTitle = "tn-content-title\">(.*?)(.*)";
        private readonly string regexDate = "<span class=\"tn-hidden\">(.*),(.*)";
        private readonly string regexNews = "<p>.*";
        private const string domainLink = "https://tengrinews.kz";
        public List<News> NewsCollection { get; set; } = new();
        public Parser()
        {
        }
        public void StartParse(DateTime lastNewsDate)
        {
            string mainLink = GetNews($"{domainLink}/kazakhstan_news");
            foreach (var item in GetDataWithRegex(mainLink, regexLink))
            {
                var singleNewsTitle = GetNews($"{domainLink}{item.Groups[1].ToString().Trim('"')}");
                var title = GetDataWithRegex(singleNewsTitle, regexTitle);
                var time = GetDataWithRegex(singleNewsTitle, regexDate);
                var texts = GetDataWithRegex(singleNewsTitle, regexNews);

                string titleNews = default;
                DateTime timeNews = default;
                string textNews = default;

                foreach (var titleItem in title)
                {
                    titleNews = titleItem.Groups[2].ToString().Replace("&quot;", "");
                }
                foreach (var textItem in texts)
                {
                    string result = textItem.Groups[0].ToString().Replace("&quot;", "").Replace("&nbsp;", "").Replace("'", "`");
                    textNews += StripHtmlTagsUsingRegex(result);
                }
                foreach (var timeItem in time)
                {
                    var date = $"{timeItem.Groups[1].ToString().Replace("Вчера", DateTime.Today.AddDays(-1).ToString("D"))} {timeItem.Groups[2]}";
                    timeNews = Helper.ConvertDateToString(date);
                }
                News news = new News(titleNews, textNews, timeNews);
                if (IsDublicateTitle(news, lastNewsDate))
                {
                    NewsCollection.Add(news);
                    Console.WriteLine($"News {news.Title} was added");
                }
                continue;
            }
            Console.WriteLine($"Parsing complete {DateTime.Now}");
        }

        private string GetNews(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                var response = client.GetAsync(url).Result;
                return response.Content.ReadAsStringAsync().Result;
            }
        }
        private IEnumerable<Match> GetDataWithRegex(string text, string regexPattern)
        {
            Regex regex = new Regex(regexPattern);
            MatchCollection matches = regex.Matches(text);
            return matches;


        }
        private bool IsDublicateTitle(News news, DateTime date)
        {
            if (date == DateTime.MinValue)
            {
                return true;
            }
            else
            {
                return news.Date > date;
            }

        }
        private string StripHtmlTagsUsingRegex(string inputString)
        {
            return Regex.Replace(inputString, @"<[^>]*>", string.Empty);
        }
    }
}

