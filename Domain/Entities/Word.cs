﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class Word
    {
        public string SingleWord { get; private set; }
        public Word(string singleWord)
        {
            SingleWord = singleWord;
        }
    }
}
