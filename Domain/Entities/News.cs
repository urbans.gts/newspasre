﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class News
    {
        public News(string title, string text, DateTime date)
        {
            Title = title;
            Text = text;
            Date = date;
        }
        public string Title { get; private set; }
        public string Text { get; private set; }
        public DateTime Date { get; private set; }
    }
}
