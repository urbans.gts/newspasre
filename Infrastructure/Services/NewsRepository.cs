﻿using ApplicationCore.Entities.AppEntities;
using ApplicationCore.Entities.Interfaces;
using Infrastructure.DataAccess;
using Infrastructure.Helpers;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;


namespace Infrastructure.Services
{
    public class NewsRepository : BaseRepository,INewsRepository
    {
        public List<News> GetDataWithDateFilter(DateTime dateTimeFrom, DateTime dateTimeTo)
        {
            if(dateTimeTo == DateTime.MinValue)
            {
                dateTimeTo = DateTime.Now;
            }
            if(dateTimeFrom == DateTime.MinValue)
            {
                dateTimeFrom = GetFistDateFromDatabase();
            }
            try
            {
                List<News> list = new List<News>();
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    string query = @"SELECT * FROM dbo.News 
                                WHERE news_date >= @news_date_from AND news_date <= @news_date_to";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@news_date_from", dateTimeFrom));
                        command.Parameters.Add(new SqlParameter("@news_date_to", dateTimeTo));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string title = reader["title"].ToString();
                                string newstext = reader["news_text"].ToString();
                                DateTime newsdate = Helper.ConvertDateToString(reader["news_date"].ToString());

                                list.Add(new News(title, newstext, newsdate));
                            }
                        }
                    }
                }
                return list;
            }
            catch (SqlException e)
            {

                throw e;
            }
        }

        public List<News> GetDataWithSearch(string text)
        {
            try
            {
                List<News> list = new List<News>();

                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    string query = @"SELECT * FROM News 
                                WHERE news_text Like @news_text";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@news_text", $"%{text}%"));
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string title = reader["title"].ToString();
                                string newstext = reader["news_text"].ToString();
                                DateTime newsdate = Helper.ConvertDateToString(reader["news_date"].ToString());

                                list.Add(new News(title, newstext, newsdate));
                            }
                        }
                    }
                }
                return list;
            }
            catch (SqlException e)
            {

                throw e;
            }
        }
        public List<Word> GetTopTenWords()
        {
            try
            {
                List<Word> list = new List<Word>();
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    string query = @"SELECT TOP 10 word,COUNT(word) AS amount
                                  FROM [NewsDatabase].[dbo].[Words]
                                  WHERE LEN(word) > 2
                                  GROUP BY word
                                  ORDER BY COUNT(*) DESC";
                    using (var command = new SqlCommand(query, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string word = reader["word"].ToString();
                                int amount = Int32.Parse(reader["amount"].ToString());

                                list.Add(new Word(word, amount));
                            }
                        }
                    }
                }
                return list;
            }
            catch (SqlException e)
            {

                throw e;
            }
        }
        private DateTime GetFistDateFromDatabase()
        {
            try
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();

                    string query = @"SELECT TOP 1 news_date 
                               FROM News 
                               ORDER BY news_date ASC";
                    using (var command = new SqlCommand(query, connection))
                    {
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                return (DateTime)reader["news_date"];

                            }
                        }
                    }
                }
                return default;
            }
            catch (SqlException e)
            {

                throw e;
            }
        }

    }
}
