﻿using System;

namespace Infrastructure.Helpers
{
    public class Helper
    {
        public static DateTime ConvertDateToString(string date)
        {
            try
            {
                return DateTime.Parse(date);
            }
            catch (FormatException)
            {
                Console.WriteLine("Unable to parse the specified date");
            }
            return default;
        }
    }
}
