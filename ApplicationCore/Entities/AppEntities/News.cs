﻿using System;

namespace ApplicationCore.Entities.AppEntities
{
    public class News
    {
        public News(string title, string text, DateTime date)
        {
            Title = title;
            Text = text;
            Date = date;
        }
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}
