﻿namespace ApplicationCore.Entities.AppEntities
{
    public class Word
    {
        public Word(string singleWord, int amount)
        {
            SingleWord = singleWord;
            Amount = amount;
        }

        public string SingleWord { get; set; }
        public int Amount { get; set; }
        
    }
}
