﻿using ApplicationCore.Entities.AppEntities;
using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities.Interfaces
{
    public interface INewsRepository
    {
        List<News> GetDataWithDateFilter(DateTime dateTimeFrom, DateTime dateTimeTo);
        List<News> GetDataWithSearch(string text);
        List<Word> GetTopTenWords();
    }
}
